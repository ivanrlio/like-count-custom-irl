import { withPluginApi } from 'discourse/lib/plugin-api';
import PostMenuComponent from 'discourse/components/post-menu';
import { Button } from 'discourse/components/post-menu';
import TopicRoute from 'discourse/routes/topic';

function initializePlugin(api) {

  let renderCustomLikeCount = function(dec, post) {
    let likeCount = dec.attrs.likeCount > 0 ? dec.attrs.likeCount.toString() : ""
      if (likeCount != "") {
        return dec.h('button.like-count-custom-btn', [
                  dec.h('span.like-count-custom-heart-container', dec.h('i.fa-heart.fa')),
                  dec.h('span.like-count-custom-val', " " + likeCount)
                 ]);
      }
  };

  api.decorateWidget('post-contents:after-cooked', dec => {
    const post = dec.getModel();
    return renderCustomLikeCount(dec, post);
  })

  $(document).on('click', '.post-controls .actions .widget-button:first-child', function(){
    if(!Discourse.User.current()) {
      var query = location.href;
      location.href = 'https://twobyfore.com/join/?try=comment' + '&redirectTo=' + query;
    }
  });
}

export default {
  name: 'like-count-custom',
  initialize: function() {
    withPluginApi('0.1', api => initializePlugin(api));
  }
  
}
